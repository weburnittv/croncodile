import dramatiq
import sys


@dramatiq.actor(queue_name="square", actor_name="square")
def square(x):
    return int(x) * int(x)


if __name__ == '__main__':
    square.send(sys.argv[1])
