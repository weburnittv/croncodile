import unittest
import mock
from croncodile.broker import Broker


class BrokerImplementation(Broker):
    def declare_queue(self, queue_name):
        pass

    def enqueue(self, message, delay=None):
        """Implement concrete enqueue function"""

        return True

    def consume(self):
        """Consume Queue"""
        pass


class FailBrokerImplementation(Broker):
    def declare_queue(self, queue_name):
        pass

    def enqueue(self, message, delay=None):
        """Implement concrete enqueue function"""

        raise Exception('Fail Broker')

    def consume(self):
        """Consume Queue"""
        pass


class BrokerTest(unittest.TestCase):
    broker = None

    def setUp(self):
        super(BrokerTest, self).setUp()

        self.broker = BrokerImplementation()

    def test_broker_send_successfully(self):
        broker = FailBrokerImplementation()
        broker.delegator(self.broker)
        result = broker.broadcast(mock.Mock())

        assert result is True

    @mock.patch('croncodile.brokers.model')
    def test_broker_sending_on_error_and_sqlite_model_handover(self, model):
        broker = FailBrokerImplementation()
        broker.delegator(FailBrokerImplementation())
        message = mock.Mock()
        result = broker.broadcast(message)
        model.connect.assert_called()
        model.save.assert_called_with(message)

        assert result is False


if __name__ == '__main__':
    unittest.main()
