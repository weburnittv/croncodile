import unittest
import os
from croncodile.brokers.model import connect, Connector, NodeRecord, save
from croncodile.core.message import Message
import uuid
import time


class CoreConnectorTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(CoreConnectorTest, cls).setUpClass()
        if os.path.exists(os.getcwd() + '/' + 'inspector.db'):
            os.remove(os.getcwd() + '/' + 'inspector.db')

    def setUp(self):
        super(CoreConnectorTest, self).setUp()
        connector, db = connect()
        self.connector = connector
        self.db = db
        self.db.begin()
        self.message = Message(
            queue_name='queue',
            actor_name='actor',
            args=(2,),
            kwargs={},
            options={},
            message_id=str(uuid.uuid4()),
            message_timestamp=time.time()
        )

    def tearDown(self):
        super(CoreConnectorTest, self).tearDown()
        self.db.rollback()

    @classmethod
    def tearDownClass(cls):
        super(CoreConnectorTest, cls).tearDownClass()
        os.remove(os.getcwd() + '/' + 'inspector.db')

    def test_connection_add(self):
        """Assure sqlite model can serialize and deserialize message correctly"""
        save(self.message)
        items = self.connector.select()

        item = items[0].message()

        assert items.count() == 1
        assert item.queue_name == self.message.queue_name
        assert item.args == self.message.args
        assert item.message_id == self.message.message_id
        assert item.message_timestamp == self.message.message_timestamp

    def test_connection_delete(self):
        item = self.connector.add(self.message)
        Connector.delete(item.id)
        items = NodeRecord.select().count()
        assert items == 0


if __name__ == 'main':
    unittest.main()
