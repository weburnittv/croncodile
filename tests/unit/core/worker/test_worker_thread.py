import threading
import time
import unittest
import uuid
from Queue import PriorityQueue

import mock
from tl.testing.thread import ThreadJoiner, ThreadAwareTestCase

from croncodile.broker import MessageProxy
from croncodile.core.message import Message
from croncodile.core.worker import WorkerThread

#  Some message in queue
message = mock.Mock()
message.queue_name = 'default_queue'

first_retry = Message(
    queue_name='queue_testing',
    actor_name='actor',
    args=(2,),
    kwargs={},
    options={'eta': 10, 'max_retries': 3, 'attempt': 2},
    message_id='id-of-message',
    message_timestamp=time.time()
)

message = Message(
    queue_name='queue_testing',
    actor_name='fail_actor',
    args=(2,),
    kwargs={},
    options={},
    message_id=str(uuid.uuid4()),
    message_timestamp=time.time()
)

handled_first_test = False


def mock_actor_square(x):
    global handled_first_test
    handled_first_test = True
    return x * x


class WorkerThreadTest(ThreadAwareTestCase):
    def test_worker_handle_message(self):
        logger = mock.Mock()
        broker = mock.Mock()
        broker.get_actor.return_value = mock_actor_square
        worker_queue = PriorityQueue()
        msg = MessageProxy(message)
        worker_queue.put((1, msg))
        consumer = mock.Mock()
        worker = WorkerThread(broker,
                              logger,
                              consumers={'queue_testing': consumer},
                              work_queue=worker_queue,
                              worker_timeout=100,
                              daemon=True)
        worker.start()
        worker.pause()
        worker.resume()
        worker.stop()

    def test_worker_process_failed_message(self):
        broker = mock.Mock()
        broker.get_actor.return_value = mock_actor_square
        logger = mock.Mock()
        worker_queue = mock.Mock()
        consumer = mock.Mock()
        worker = WorkerThread(broker,
                              logger,
                              consumers={'queue_testing': consumer},
                              work_queue=worker_queue,
                              worker_timeout=100,
                              daemon=True)
        msg = MessageProxy(first_retry)
        attempt = msg.options['attempt']
        worker.process_message(MessageProxy(first_retry))
        self.assertEqual(attempt + 1, msg.options['attempt'], 'Attempt must increase one')
        consumer.post_process_message.assert_called_with(msg)
        worker_queue.task_done.assert_called()


with ThreadJoiner(0.5):
    thread = threading.Thread(target=unittest.makeSuite(WorkerThreadTest))
    thread.start()
    thread.join(0)
