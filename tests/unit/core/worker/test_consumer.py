import time
import unittest
import uuid
from Queue import PriorityQueue

import mock
from tl.testing.thread import ThreadJoiner
from unittest_data_provider import data_provider

from croncodile.broker import MessageProxy
from croncodile.core.message import Message
from croncodile.core.worker import ConsumerThread
from croncodile.errors import ConnectionError, ActorNotFound
from croncodile.utils import iter_queue

message = Message(
    queue_name='queue',
    actor_name='actor',
    args=(2,),
    kwargs={},
    options={},
    message_id=str(uuid.uuid4()),
    message_timestamp=time.time()
)
eta_message = Message(
    queue_name='queue',
    actor_name='actor',
    args=(2,),
    kwargs={},
    options={'eta': 10},
    message_id=str(uuid.uuid4()),
    message_timestamp=time.time()
)
eta_delay_message = Message(
    queue_name='queue',
    actor_name='actor',
    args=(2,),
    kwargs={},
    options={'eta': int(time.time()) * 1000 + 5000},
    message_id=str(uuid.uuid4()),
    message_timestamp=time.time()
)

failure_message_received = Message(
    queue_name='queue_on_secondary',
    actor_name='secondary_actor',
    args=(2,),
    kwargs={},
    options={},
    message_id=str(uuid.uuid4()),
    message_timestamp=time.time()
)
first_retry = Message(
    queue_name='queue',
    actor_name='actor',
    args=(2,),
    kwargs={},
    options={'eta': 10, 'max_retries': 3},
    message_id=str(uuid.uuid4()),
    message_timestamp=time.time()
)
last_retry = Message(
    queue_name='queue',
    actor_name='actor',
    args=(2,),
    kwargs={},
    options={'eta': 10, 'max_retries': 3, 'attempt': 3},
    message_id=str(uuid.uuid4()),
    message_timestamp=time.time()
)


class ConsumerThreadTest(unittest.TestCase):
    request_cases = lambda: (
        ([MessageProxy(message), MessageProxy(eta_message)],),
        ([MessageProxy(eta_message).fail(), MessageProxy(eta_message).fail()],),
        ([MessageProxy(eta_delay_message), MessageProxy(eta_delay_message)],)
    )
    primary_broker_failer = lambda: ((MessageProxy(failure_message_received),),)

    def test_start_consumer_following_attempt_and_error(self):
        with ThreadJoiner(0.2):
            broker = mock.Mock()

            broker.consume.side_effect = Exception('Exception during processing consumer')
            logger = mock.Mock()
            consumer = ConsumerThread(broker=broker, logger=logger, queue_name="default", prefetch=100,
                                      work_queue=mock.Mock(), worker_timeout=100, daemon=False,
                                      attempts=1)

            consumer.start()
            # logger.error.assert_called()

    @data_provider(request_cases)
    def test_start_consumer_handle_at_least_few_message(self, messages):
        with ThreadJoiner(0.2):
            broker = mock.Mock()
            consume = mock.Mock()
            consume.__iter__ = mock.Mock(return_value=iter(messages))
            broker.consume.return_value = consume
            logger = mock.Mock()
            consumer = ConsumerThread(broker=broker, logger=logger, queue_name="default", prefetch=100,
                                      work_queue=mock.Mock(), worker_timeout=100, daemon=False,
                                      attempts=1)

            consumer.start()

    @data_provider(primary_broker_failer)
    def test_consumer_deal_with_primary_broker_error_connection(self, msg):
        with ThreadJoiner(0.2):
            broker = mock.Mock()
            primary_consume = mock.Mock()
            iterates = mock.Mock(return_value=iter([0, 1, 2]))
            iterates.side_effect = ConnectionError('Primary broker raise error')
            primary_consume.__iter__ = iterates
            broker.consume.return_value = primary_consume

            secondary_broker = mock.Mock(name='Secondary Broker')
            secondary_consumer = mock.Mock(name='Secondary Consumer')
            secondary_consumer.__iter__ = mock.Mock(return_value=iter([msg]))
            secondary_broker.consume.return_value = secondary_consumer
            broker.delegator.return_value = secondary_broker

            logger = mock.Mock()
            worker_queue = PriorityQueue()
            consumer = ConsumerThread(broker=broker, logger=logger, queue_name="default", prefetch=100,
                                      work_queue=worker_queue, worker_timeout=100, daemon=False,
                                      attempts=2)
            consumer.start()
            has_item = False
            for _, message in iter_queue(worker_queue):
                assert message == msg
                has_item = True
            self.assertTrue(has_item, "worker_queue must have msg in it")

    @mock.patch('croncodile.brokers.model')
    def test_consumer_stop_and_requeue_some_delayed_message(self, model):
        broker = mock.Mock()
        primary_consume = mock.Mock()
        iterates = mock.Mock(return_value=iter([MessageProxy(eta_delay_message), MessageProxy(eta_delay_message)]))
        primary_consume.__iter__ = iterates
        primary_consume.requeue.side_effect = ConnectionError('Requeue message raises error')
        broker.consume.return_value = primary_consume

        logger = mock.Mock()
        worker_queue = PriorityQueue()
        consumer = ConsumerThread(broker=broker, logger=logger, queue_name="default", prefetch=100,
                                  work_queue=worker_queue, worker_timeout=100, daemon=False,
                                  attempts=1)

        consumer.start()
        model.reset_mock()

    @mock.patch('croncodile.brokers.model')
    def test_consumer_handle_nack_or_ack_on_errors(self, model):
        broker = mock.Mock()
        primary_consume = mock.Mock()
        iterates = mock.Mock(return_value=iter(
            [MessageProxy(message), MessageProxy(message), MessageProxy(message)]))
        primary_consume.__iter__ = iterates
        primary_consume.nack.side_effect = ConnectionError('Raise Connection during (n)ack messages')
        broker.get_actor.side_effect = ActorNotFound('In case an (n)ack message can not served by proper Actor')
        broker.consume.return_value = primary_consume

        logger = mock.Mock()
        worker_queue = PriorityQueue()
        consumer = ConsumerThread(broker=broker, logger=logger, queue_name="default", prefetch=100,
                                  work_queue=worker_queue, worker_timeout=100, daemon=False,
                                  attempts=1)

        consumer.start()
        # logger.error.assert_called_with("Unable to ack message queues(1)")
        # model.connect.assert_called()
        model.reset_mock()

    def test_consumer_post_process_message_enqueue_failed_message(self):
        broker = mock.Mock()
        primary_consume = mock.Mock()
        iterates = mock.Mock(return_value=iter(
            [MessageProxy(first_retry), ]))
        primary_consume.__iter__ = iterates
        broker.consume.return_value = primary_consume
        logger = mock.Mock()
        worker_queue = PriorityQueue()
        thread_consumer = ConsumerThread(broker=broker, logger=logger, queue_name="default", prefetch=100,
                                  work_queue=worker_queue, worker_timeout=100, daemon=False,
                                  attempts=1)
        consumer = mock.Mock()
        thread_consumer.consumer = consumer
        msg = MessageProxy(last_retry)
        msg.fail()
        thread_consumer.post_process_message(msg)
        thread_consumer.handle_acks()

    def test_consumer_post_process_message_nack_failed_message(self):
        broker = mock.Mock()
        primary_consume = mock.Mock()
        iterates = mock.Mock(return_value=iter(
            [MessageProxy(first_retry), ]))
        primary_consume.__iter__ = iterates
        broker.consume.return_value = primary_consume
        logger = mock.Mock()
        worker_queue = PriorityQueue()
        thread_consumer = ConsumerThread(broker=broker, logger=logger, queue_name="default", prefetch=100,
                                  work_queue=worker_queue, worker_timeout=100, daemon=False,
                                  attempts=1)
        consumer = mock.Mock()
        thread_consumer.consumer = consumer
        msg = MessageProxy(first_retry)
        msg.fail()
        thread_consumer.post_process_message(msg)
        thread_consumer.handle_acks()
        consumer.nack.assert_called_with(msg)

    def test_consumer_nack_a_failed_message(self):
        pass

if __name__ == '__main__':
    unittest.main()
