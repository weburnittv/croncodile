import unittest
import mock
from croncodile.core.worker import Worker

#  Some message in queue
message = mock.Mock()
message.queue_name = 'default_queue'


class WorkerTest(unittest.TestCase):
    @mock.patch('croncodile.core.worker.WorkerThread', autospec=True)
    def test_start_pause_resume_worker(self, thread):
        tWorker = mock.Mock()
        thread.return_value = tWorker
        #  Declare broker have 2 queue
        broker = mock.Mock()
        broker.get_declared_queues.return_value = []
        broker.get_declared_delay_queues.return_value = []
        worker = Worker(broker=broker,
                        logger=mock.Mock(),
                        worker_timeout=2000,
                        worker_threads=1,
                        consumer_attempts=1)
        #  Assert Start
        worker.start()
        tWorker.start.assert_called()
        #  Assert Pause
        worker.pause()
        tWorker.pause.assert_called()
        tWorker.paused_event.wait()

        # Assert Resume
        worker.resume()
        tWorker.resume.assert_called()
        tWorker.reset_mock()

    @mock.patch('croncodile.core.worker.iter_queue', return_value=iter([(1, message)]))
    @mock.patch('croncodile.core.worker.ConsumerThread', autospec=True)
    @mock.patch('croncodile.core.worker.WorkerThread', autospec=True)
    def test_worker_create_thread_worker_consumer_and_stop(self, wthread, cthread, iter_queue):
        tWorker = mock.Mock()
        wthread.return_value = tWorker
        cWorker = mock.Mock()
        cthread.return_value = cWorker

        #  Declare broker have 2 queue
        broker = mock.Mock()
        broker.get_declared_queues.return_value = ['default_queue', 'secondary_queue', 'secondary_queue']
        broker.get_declared_delay_queues.return_value = ['delay:default_queue',
                                                         'delay:secondary_queue',
                                                         'delay:third_queue']
        broker.consume.return_value = []
        worker_threads = 2

        worker = Worker(broker=broker,
                        logger=mock.Mock(),
                        worker_timeout=2000,
                        worker_threads=worker_threads,
                        consumer_attempts=1)

        #  Assert Start
        worker.start()
        self.assertEqual(worker_threads, wthread.call_count, "No worker reflect workder_threads")
        self.assertEqual(5, cthread.call_count, "Assure worker creates no. consumer corresponding queues")

        worker.stop()
        #  Assert Consumer must requeue message while it stops
        cWorker.requeue_messages.assert_called_with([message])
        #  Assert Worker Thread and Consumer Thread call stop()
        tWorker.stop.assert_called()
        cWorker.stop.assert_called()
        cWorker.close.assert_called()

    def test_worker_restart(self):
        pass


if __name__ == '__main__':
    unittest.main()
