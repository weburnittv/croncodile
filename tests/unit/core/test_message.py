import unittest
import uuid
import time
from croncodile.core.message import Message


class MessageTest(unittest.TestCase):
    def test_init(self):
        message = Message(
            queue_name='queue',
            actor_name='actor',
            args=(2,),
            kwargs={},
            options={},
            message_id=str(uuid.uuid4()),
            message_timestamp=time.time()
        )
        assert message.queue_name == 'queue'
        message = message.copy(options={"redis_message_id": "id"})
        assert message.options["redis_message_id"] == 'id'

        encoded = message.encode()
        decoded_message = message.decode(encoded)

        assert decoded_message.queue_name == 'queue'
        assert decoded_message.options["redis_message_id"] == 'id'


if __name__ == '__main__':
    unittest.main()
