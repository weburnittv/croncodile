import unittest
import mock
from croncodile.core.player import Actor
from croncodile.errors import ConnectionError


class PlayerTest(unittest.TestCase):
    def test_actor_behave(self):
        broker = mock.Mock()
        fly = lambda x: x * x
        actor = Actor(fn=fly, broker=broker, actor_name="fly", queue_name="default")
        self.assertEqual(9, actor(3))
        broker.declare_actor.assert_called_with(actor)

    def test_actor_send_queue_success(self):
        fly = lambda x: x * x
        broker = mock.Mock()
        actor = Actor(fn=fly, broker=broker, actor_name="fly", queue_name="default")
        actor.send(2)
        broker.broadcast.assert_called()

    def test_actor_raise_connection_exception(self):
        fly = lambda x: x * x
        broker = mock.Mock()
        broker.broadcast.side_effect = ConnectionError('Connection error')
        broker.delegator.return_value = None
        with self.assertRaises(ConnectionError) as error:
            actor = Actor(fn=fly, broker=broker, actor_name="fly", queue_name="default")
            actor.send(2)

    def test_actor_raise_unknown_exception(self):
        fly = lambda x: x * x
        broker = mock.Mock()
        broker.broadcast.side_effect = Exception('Unknown exception')
        with self.assertRaises(Exception) as error:
            actor = Actor(fn=fly, broker=broker, actor_name="fly", queue_name="default")
            actor.send(2)


if __name__ == '__main__':
    unittest.main()
