from croncodile.core import task
from croncodile.core.player import Actor
import pytest
import unittest
import mock

broker = mock.MagicMock()
supervisor = mock.MagicMock()
supervisor.get_broker.return_value = broker


def create_mock():
    return supervisor


@mock.patch('croncodile.InspectorSupervisor', create=True, new_callable=create_mock)
class GenericActorTest(unittest.TestCase):
    def test_generic_actors_can_be_defined(self, stub):
        class Add(task.GenericActor):
            def perform(self, x, y):
                return x + y

        assert isinstance(Add, Actor)

        assert Add(1, 2) == 3

    def test_generic_actors_can_be_assigned_options(self, stub):
        class Add(task.GenericActor):
            class Meta:
                max_retries = 32

            def perform(self, x, y):
                return x + y

        assert Add.options["max_retries"] == 32

    def test_generic_actors_raise_not_implemented_if_perform_is_missing(self, stub):
        class Foo(task.GenericActor):
            pass

        with pytest.raises(NotImplementedError):
            Foo()

    def test_generic_actors_can_be_abstract(self, stub):
        calls = set()

        class BaseTask(task.GenericActor):
            class Meta:
                abstract = True
                queue_name = "tasks"

            def get_task_name(self):
                raise NotImplementedError

            def perform(self):
                calls.add(self.get_task_name())

        assert not isinstance(BaseTask, Actor)

        # subclass BaseTask
        class FooTask(BaseTask):
            def get_task_name(self):
                return "Foo"

        class BarTask(BaseTask):
            def get_task_name(self):
                return "Bar"

        class LastTask(BaseTask):
            def get_task_name(self):
                return 'Last'

        assert FooTask.queue_name == BarTask.queue_name == "tasks"
        self.assertEqual(3, supervisor.call_count, "Supervisor must call 3 times corresponding 3 Task class")
        FooTask()
        BarTask()
        LastTask()

        assert calls == {"Foo", "Bar", "Last"}


if __name__ == '__main__':
    unittest.main()
