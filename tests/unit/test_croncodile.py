import croncodile as croncodile
import unittest
import mock
from croncodile.core.player import Actor


class InspecronTest(unittest.TestCase):
    def test_singleton(self):
        with mock.patch('croncodile.brokers.irabbitmq.RabbitMQ') as rabbit:
            with mock.patch('croncodile.brokers.iredis.Redis') as redis:
                logger = mock.Mock()
                latest_cron = croncodile.init_croncodile(logger)
                cronize = croncodile.manager
                assert cronize == latest_cron

    def test_manager_define_actor(self):
        logger = mock.Mock()
        cronize = croncodile.init_croncodile(logger)
        assert cronize.logger == logger
        mock_broker = mock.Mock()
        second_broker = mock.Mock()
        cronize.register_broker(mock_broker, "rabbitmq")
        cronize.register_broker(second_broker, "redis")
        mock_broker.delegator.assert_called_with(second_broker)
        assert cronize.get_broker("rabbitmq") == mock_broker

        @croncodile.actor(actor_name='math.add', queue_name="math_add")
        def inline_actor_math_add(x, y):
            return x + y

        self.assertIsInstance(inline_actor_math_add, Actor)
        inline_actor_math_add.send(2, 2)
        mock_broker.declare_actor.assert_called()
        self.assertEqual(4, inline_actor_math_add(2, 2), "Actor can perform correct decorated function")

        # mock_broker.declare_actor.assert_called()  # Assert broker must declare actor
        # mock_broker.broadcast.assert_called()  # Assert broker must enqueue message


if __name__ == '__main__':
    unittest.main()
