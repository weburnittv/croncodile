import mock
import unittest
import os
from tl.testing.thread import ThreadJoiner, ThreadAwareTestCase
from croncodile.core.worker import Worker
from croncodile import init_croncodile, default_config

logger = mock.MagicMock()


class SecondaryBrokerTest(ThreadAwareTestCase):
    def test_secondary_rabbit_broker_consume_message_correctly(self):
        rabbit = default_config['rabbitmq']
        os.environ['AUTO_DISCOVER_CONFIG'] = '0'

        manager = init_croncodile(logger=logger, config={'rabbitmq': rabbit})
        from croncodile.examples.component_tasks import ModelData, ProcessingTask
        model = ModelData("Paul", 20)
        ProcessingTask.send(model, "SWAT Member")
        worker = Worker(manager.get_broker('redis'), logger)
        # worker.start()
        # worker.stop()


if __name__ == '__main__':
    unittest.main()
