import mock
import unittest
import os
from tl.testing.thread import ThreadJoiner, ThreadAwareTestCase
from croncodile.core.worker import Worker
from croncodile import init_croncodile, default_config

logger = mock.MagicMock()


class PrimaryBrokerTest(ThreadAwareTestCase):
    def test_primary_redis_broker_consume_message_correctly(self):
        with ThreadJoiner(0.5):
            redis = default_config['redis']
            os.environ['AUTO_DISCOVER_CONFIG'] = '0'

            manager = init_croncodile(logger=logger, config={'redis': redis})
            from croncodile.examples.component_tasks import ModelData, ProcessingTask
            model = ModelData("Paul", 20)
            ProcessingTask.send(model, "SWAT Member")
            worker = Worker(manager.get_broker('redis'), logger)
            # worker.start()
            # worker.stop()


if __name__ == '__main__':
    unittest.main()
