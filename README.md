#CronJob Features
* CronJobs centralize our application through Task
* CronJobs will automatically discover broker server
* Task is implemented respectively following our Business requirement
* Task is minimal enough to implement Unit Test

#Usage
+ **Function can be an Actor by `@decorator` (Not Recommended)**
```python
import croncodile

@croncodile.actor(actor_name='square', queue_name='default_queue')
def square(x):
    print('Processing Square %d' % int(x))
    return x * x

```
+ **Implement Task as an Actor (RECOMMEND)**
```python
from croncodile.core.task import GenericActor


class ProcessingTask(GenericActor):
    """Must implement GeneratorActor"""
    class Meta:
        actor_name = "process_task"
        queue_name = "process_task"

    def perform(self, model, role):
        """
        :type model: ModelData
        :param model: Model
        :type role: str
        :param role: New role
        :return:
        """
        model.promote(role)
        print("After promote: " + role)
        print(model.name)
        print(model.age)


class ModelData(object):
    """DTO(Data Transfer Object) wrap data for Processing Task"""
    description = "Basic"

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def promote(self, role):
        self.description = role
```

Sending Task to Cron Server

```python
from examples.models import ModelData, ProcessingTask, square
slaver = ModelData("Paul", 32)
#In your controller
def assignment_create_action():
    ProcessingTask.send(slaver, "Guild Master") # respect ProcessingTask.perform's parameters
    # square is wrapped by Actor thus we use it as an Actor
    square.send(10) # respect square's function parameters
```
**Best Practices**

* Your Task must have Unit Test to protect behavior of `perform()` function
* Your Controller's action must have proper Unit Test to protect(2 cases) whether `ProcessingTask.send()` is invoked or not
