from setuptools import setup

setup(
    name='croncodile',
    version='0.0.1',
    author='Paul Aan',
    author_email='hoaian@inspectorio.com',
    packages=['croncodile', 'croncodile.brokers', 'croncodile.core', 'croncodile.examples'],
    include_package_data=True,
    package_dir={'croncodile': 'croncodile'},
    package_data={'croncodile': ['brokers/redis/*.lua']},
    install_requires=[
        'redis',
        'pika',
        'cython',
        'selectors2',
        'peewee',
        'apsw',
        'python-json-logger'
    ],
)
