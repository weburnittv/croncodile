import time
from Queue import Empty, PriorityQueue, Queue
from collections import defaultdict
from itertools import chain
from threading import Event, Thread

from croncodile.errors import ActorNotFound, ConnectionError
from croncodile.utils import compute_backoff, current_millis, iter_queue, join_all, q_name

CONSUMER_ATTEMPT = 9999


class Worker:
    """Worker has many Threads(WorkerThread) in which, it will consume primary broker

    Parameters:
      broker(Broker)
      worker_timeout(int): The number of milliseconds workers should
        wake up after if the queue is idle.
      worker_threads(int): The number of worker threads to spawn.
    """

    def __init__(self, broker, logger, worker_timeout=1000, worker_threads=8, consumer_attempts=CONSUMER_ATTEMPT):
        self.consumer_attempts = consumer_attempts
        self.logger = logger
        self.broker = broker

        self.consumers = {}
        self.queue_prefetch = min(worker_threads * 2, 65535)
        self.delay_prefetch = min(worker_threads * 1000, 65535)

        self.workers = []
        self.work_queue = PriorityQueue()
        self.worker_timeout = worker_timeout
        self.worker_threads = worker_threads

    def start(self):
        """Initialize the worker boot sequence and start up all the
        worker threads.
        """
        worker_middleware = _WorkerMiddleware(self)

        for queue_name in self.broker.get_declared_queues():
            worker_middleware.after_declare_queue(self.broker, queue_name)

        for queue_name in self.broker.get_declared_delay_queues():
            worker_middleware.after_declare_delay_queue(self.broker, queue_name)

        for _ in range(self.worker_threads):
            self._add_worker()

    def pause(self):
        """Pauses all the worker threads.
        """
        for worker in self.workers:
            worker.pause()

        for worker in self.workers:
            worker.paused_event.wait()

    def resume(self):
        """Resumes all the worker threads.
        """
        for worker in self.workers:
            worker.resume()

    def stop(self, timeout=600000):
        """Gracefully stop the Worker and all of its consumers and
        workers.

        Parameters:
          timeout(int): The number of milliseconds to wait for
            everything to shut down.
        """
        for thread in chain(self.workers, self.consumers.values()):
            thread.stop()

        join_all(chain(self.workers, self.consumers.values()), timeout)
        messages_by_queue = defaultdict(list)
        queues = iter_queue(self.work_queue)
        for _, message in queues:
            messages_by_queue[message.queue_name].append(message)

        for queue_name, messages in messages_by_queue.items():
            try:
                self.consumers[queue_name].requeue_messages(messages)
            except Exception as e:  # pragma: no cover
                self.logger.error("Failed to requeue messages on queue %s (Error: %s)" % (queue_name, e.message),
                                  queue_name, exc_info=True)  # pragma: no cover

        for consumer in self.consumers.values():
            consumer.close()

        self.logger.info("Worker has been shut down.")

    def _add_consumer(self, queue_name, delay=False):
        if queue_name in self.consumers:
            return

        consumer = self.consumers[queue_name] = ConsumerThread(
            broker=self.broker,
            logger=self.logger,
            queue_name=queue_name,
            prefetch=self.delay_prefetch if delay else self.queue_prefetch,
            work_queue=self.work_queue,
            worker_timeout=self.worker_timeout,
            attempts=self.consumer_attempts,
            daemon=True
        )
        consumer.start()

    def _add_worker(self):
        worker = WorkerThread(
            broker=self.broker,
            logger=self.logger,
            consumers=self.consumers,
            work_queue=self.work_queue,
            worker_timeout=self.worker_timeout,
            daemon=True
        )
        worker.start()
        self.workers.append(worker)


class _WorkerMiddleware(object):
    """Middleware is used to exchange information between worker and broker"""

    def __init__(self, worker):
        self.logger = worker.logger
        self.worker = worker

    def after_declare_queue(self, broker, queue_name):
        self.worker._add_consumer(queue_name)

    def after_declare_delay_queue(self, broker, queue_name):
        self.worker._add_consumer(queue_name, delay=True)


class ConsumerThread(Thread):
    """Consumer will maintain primary broker and deliver MESSAGE to work_queue
    By then, worker will handle by its own process
    """
    current_broker = None

    def __init__(self, broker, logger, queue_name, prefetch, work_queue, worker_timeout, daemon=True,
                 attempts=CONSUMER_ATTEMPT):
        super(ConsumerThread, self).__init__()

        self.daemon = daemon
        self.logger = logger
        self.running = False
        self.consumer = None
        self.broker = broker
        self.prefetch = prefetch
        self.queue_name = queue_name
        self.work_queue = work_queue
        self.worker_timeout = worker_timeout
        self.delay_queue = PriorityQueue()
        self.acks_queue = Queue()
        self.attempts = attempts

    def run(self, attempts=0, current=0, broker=None):
        if not broker:
            broker = self.broker
        if attempts == 0:
            attempts = self.attempts
        self.running = True
        if current > attempts:
            self.stop()
            return
        try:
            if broker:
                self.consumer = self._current_broker.consume(
                    queue_name=self.queue_name,
                    prefetch=self.prefetch,
                    timeout=self.worker_timeout,
                )
                for message in self.consumer:
                    if message is not None:
                        self.handle_message(message)
                    self.handle_acks()
                    self.handle_delayed_messages()
                    if not self.running:
                        break

        except ConnectionError as c_error:
            self.logger.error("Consumer encountered a connection error(%s)" % c_error.message)
            """Switch to next broker to consume message"""
            self.current_broker = self._current_broker.delegator()
        except Exception as e:
            self.logger.error("Consumer encountered an error(%s)" % e.message, exc_info=True)

        # The consumer must retry itself with exponential backoff
        # assuming it hasn't been shut down.
        if self.running:
            current, backoff_ms = compute_backoff(current, jitter=False, factor=100, max_backoff=60000)
            time.sleep(backoff_ms / 1000)
            return self.run(attempts=self.attempts, current=current + 1, broker=self.current_broker)

    @property
    def _current_broker(self):
        if self.current_broker:
            return self.current_broker
        return self.broker

    def handle_acks(self):
        """Perform any pending (n)acks.
        """
        try:
            for message in iter_queue(self.acks_queue):
                if message.failed:
                    self.consumer.nack(message)
                else:
                    self.consumer.ack(message)
                self.acks_queue.task_done()
        except ConnectionError as e:
            import croncodile.brokers.model as model
            self.acks_queue.put(message)
            self.logger.error("Unable to ack message queues(%d)" % self.acks_queue.qsize(), exc_info=True)
            model.connect()
            for message in iter_queue(self.acks_queue):
                action = "ack"
                if message.failed:
                    action = "nack"
                model.save(message, action=action)
                self.acks_queue.task_done()

    def handle_delayed_messages(self):
        """Enqueue any delayed messages whose eta has passed.
        """
        for eta, message in iter_queue(self.delay_queue):
            if eta > current_millis():  # Message is not supposed to be processed for now
                self.delay_queue.put((eta, message))  # Push it back to delay queue
                self.delay_queue.task_done()
                break
            # Time to process message right now
            queue_name = q_name(message.queue_name)
            new_message = message.copy(queue_name=queue_name)
            del new_message.options["eta"]

            self._current_broker.enqueue(new_message)
            self.post_process_message(message)
            self.delay_queue.task_done()

    def handle_message(self, message):
        """Handle a message received off of the underlying consumer.
        If the message has an eta, delay it.  Otherwise, put it on the
        work queue.
        """
        try:
            if "eta" in message.options:
                self.delay_queue.put((message.options.get("eta", 0), message))
            else:
                actor = self._current_broker.get_actor(message.actor_name)
                self.work_queue.put((actor.priority, message))
        except ActorNotFound:
            self.logger.error(
                "Received message for undefined actor %r. Moving it to the DLQ.",
                message.actor_name, exc_info=True
            )
            message.fail()
            self.post_process_message(message)

    def post_process_message(self, message):
        """Called by worker threads whenever they're done processing
        individual messages, signaling that each message is ready to
        be acked or rejected.
        """
        self.acks_queue.put(message)
        self.consumer.interrupt()

    def requeue_messages(self, messages):
        """Called on worker shutdown and whenever there is a
        connection error to move unacked messages back to their
        respective queues asap.
        """
        try:
            self.consumer.requeue(messages)
        except ConnectionError as e:
            import croncodile.brokers.model as model
            self.logger.error("Can not requeue messages(Error:%s)" % e.message, exc_info=True)
            model.connect()
            for message in messages:
                model.save(message, action="requeue")

    def stop(self):
        """Initiate the ConsumerThread shutdown sequence.

        Code calling this method should then join on the thread and
        wait for it to finish shutting down.
        """
        self.close()
        self.logger.debug("Stopping consumer thread...")
        self.running = False

    def close(self):
        """Close this consumer thread and its underlying connection.
        """
        if self.consumer:
            self.handle_acks()
            self.requeue_messages(m for _, m in iter_queue(self.delay_queue))
            self.consumer.close()


class WorkerThread(Thread):
    """WorkerThreads process incoming messages off of the work queue
    on a loop.  By themselves, they don't do any sort of network IO.

    Parameters:
      broker(Broker)
      consumers(dict[str, ConsumerThread])
      work_queue(Queue)
      worker_timeout(int)
    """

    def __init__(self, broker, logger, consumers, work_queue, worker_timeout, daemon=True):
        super(WorkerThread, self).__init__()

        self.logger = logger
        self.running = False
        self.paused = False
        self.paused_event = Event()
        self.broker = broker
        self.consumers = consumers
        self.work_queue = work_queue
        self.timeout = worker_timeout / 1000
        self.daemon = daemon

    def run(self):
        self.running = True
        while self.running:
            if self.paused:
                self.paused_event.set()  # pragma: no cover
                time.sleep(self.timeout)  # pragma: no cover
                continue  # pragma: no cover

            try:
                # The is that Worker have a queue which will receive messages from consumer
                _, message = self.work_queue.get(timeout=self.timeout)
                self.process_message(message)
            except Empty:  # pragma: no cover
                pass  # pragma: no cover
            except Exception as e:  # pragma: no cover
                self.logger.error(e.message, exc_info=True)  # pragma: no cover

    def process_message(self, message):
        """Process a message pulled off of the work queue then push it
        back to its associated consumer for post processing.

        Parameters:
          message(MessageProxy)
        """
        try:
            if int(message.options.get('attempt', 0)) < int(message.options.get('max_retries', 5)):
                actor = self.broker.get_actor(message.actor_name)
                actor(*message.args, **message.kwargs)

        except Exception as e:  # pragma: no cover
            message.fail()
            self.logger.error(e)

        finally:
            message.options.update({'attempt': int(message.options.get('attempt', 0)) + 1})
            self.consumers[message.queue_name].post_process_message(message)
            self.work_queue.task_done()

    def pause(self):
        """Pause this worker.
        """
        self.paused = True
        self.paused_event.clear()

    def resume(self):
        """Resume this worker.
        """
        self.paused = False
        self.paused_event.clear()

    def stop(self):
        """Initiate the WorkerThread shutdown process.

        Code calling this method should then join on the thread and
        wait for it to finish shutting down.
        """
        self.running = False
