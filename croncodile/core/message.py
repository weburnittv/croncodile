import cPickle as pickle
import time
import uuid
from collections import namedtuple


class Message(namedtuple("Message", (
        "queue_name", "actor_name", "args", "kwargs",
        "options", "message_id", "message_timestamp",
))):
    """Encapsulates metadata about messages being sent to individual actors.

    Parameters:
      queue_name(str): The name of the queue the message belogns to.
      actor_name(str): The name of the actor that will receive the message.
      args(tuple): Positional arguments that are passed to the actor.
      kwargs(dict): Keyword arguments that are passed to the actor.
      options(dict): Arbitrary options passed to the broker and middleware.
      message_id(str): A globally-unique id assigned to the actor.
      message_timestamp(int): The UNIX timestamp in milliseconds
        representing when the message was first enqueued.
    """
    failed = False

    def __new__(cls, queue_name, actor_name, args, kwargs, options=None, message_id=None, message_timestamp=None):
        return super(Message, cls).__new__(
            cls, queue_name, actor_name, tuple(args), kwargs, options,
            message_id=message_id or uuid.uuid4(),
            message_timestamp=message_timestamp or int(time.time() * 1000),
        )

    @classmethod
    def decode(cls, data):
        """Convert a JSON bytestring to a message.
        """
        return pickle.loads(data)

    def encode(self):
        """Convert this message to a JSON bytestring.
        """
        return pickle.dumps(self)

    def copy(self, **attributes):
        """Create a copy of this message.
        """
        updated_options = attributes.pop("options", {})
        options = attributes["options"] = self.options.copy()
        options.update(updated_options)
        replaced = self._replace(**attributes)
        return replaced
