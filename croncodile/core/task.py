from croncodile import actor

MAX_RETRIES = 10


class AbstractActor(type):
    """Meta for class-based actors.
    """

    def __new__(cls, name, bases, attrs):
        clazz = super(AbstractActor, cls).__new__(cls, name, bases, attrs)
        meta = getattr(clazz, "Meta", object())
        if not getattr(meta, "abstract", False):
            options = {name: getattr(meta, name) for name in vars(meta) if not name.startswith("_")}
            options.pop("abstract", False)
            if not options.get('max_retries', None):
                options['max_retries'] = MAX_RETRIES
            return actor(clazz(), **options)

        setattr(meta, "abstract", False)
        return clazz


class GenericActor:
    __metaclass__ = AbstractActor
    """Base-class for class-based actors.
    Each task must implement this Actor to process on their own
    """

    class Meta:
        abstract = True

    @property
    def __name__(self):
        """The default name of this actor.
        """
        return type(self).__name__

    def __call__(self, *args, **kwargs):
        return self.perform(*args, **kwargs)

    def perform(self):
        """This is the method that gets called when the actor receives
        a message.  All non-abstract subclasses must implement this
        method.
        """
        raise NotImplementedError("%s does not implement perform()" % self.__name__)
