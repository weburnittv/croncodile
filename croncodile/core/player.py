from message import Message
from croncodile.errors import ConnectionError, UndefinedBrokerError
import uuid
import time


class Actor:
    """Thin wrapper around callables that stores metadata about how
    they should be executed asynchronously.
    """

    def __init__(self, fn, broker, actor_name, queue_name, options=None, priority=1):
        """

        :param fn:
        :type broker: croncodile.broker.Broker
        :param broker:
        :param actor_name:
        :param queue_name:
        :param priority:
        """
        self.fn = fn
        self.broker = broker
        self.actor_name = actor_name
        self.queue_name = queue_name
        self.priority = priority
        self.options = options
        self.broker.declare_actor(self)

    def send(self, *args, **kwargs):
        """Asynchronously send a message to this actor.

        Note:
          All arguments must be JSON-encodable.

        Parameters:
          \*args(tuple): Positional arguments to send to the actor.
          \**kwargs(dict): Keyword arguments to send to the actor.

        Returns:
          Message: The enqueued message.
        """
        return self.send_with_options(args=args, kwargs=kwargs)

    def send_with_options(self, args=None, kwargs=None, delay=None):
        """Asynchronously send a message to this actor, along with an
        arbitrary set of processing options for the broker and
        middleware.

        Parameters:
          args(tuple): Positional arguments that are passed to the actor.
          kwargs(dict): Keyword arguments that are passed to the actor.
          delay(int): The minimum amount of time, in milliseconds, the
            message should be delayed by.
          \**options(dict): Arbitrary options that are passed to the
            broker and any registered middleware.

        Returns:
          Message: The enqueued message.
        """
        message = Message(
            queue_name=self.queue_name,
            actor_name=self.actor_name,
            args=args or (),
            kwargs=kwargs or {},
            options=self.options,
            message_id=str(uuid.uuid4()),
            message_timestamp=time.time()
        )
        processed = False
        while not processed:  # Assure broker must deal with failure by next broker
            try:
                self.broker.broadcast(message, delay=delay)
                processed = True
            except Exception as error:
                raise error

        return processed

    def __call__(self, *args, **kwargs):
        """Process behavior
        """
        return self.fn(*args, **kwargs)

    def __repr__(self):  # pragma: no cover
        return "Actor(queue_name={0}, actor_name={1})".format(self.queue_name, self.actor_name)

    def __str__(self):  # pragma: no cover
        return "Actor(queue_name={0}, actor_name={1})".format(self.queue_name, self.actor_name)
