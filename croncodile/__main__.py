import argparse
import importlib
import logging
import multiprocessing
import signal
import sys
import time

import os

from croncodile import __version__, logger
from croncodile.core.worker import Worker
from croncodile.errors import ConnectionError

try:
    import watchdog.events
    import watchdog.observers

    HAS_WATCHDOG = True
except ImportError:  # pragma: no cover
    HAS_WATCHDOG = False

running = False
#: The number of available cpus.
cpus = multiprocessing.cpu_count()

#: The logging format.
logformat = "[%(asctime)s] [PID %(process)d] [%(threadName)s] [%(name)s] [%(levelname)s] %(message)s"

#: The logging verbosity levels.
verbosity = {
    0: logging.INFO,
    1: logging.DEBUG,
}


def import_broker(value):
    modname, broker = value, None
    if ":" in value:
        modname, broker = value.split(":", 1)

    module = importlib.import_module(modname)
    from croncodile import manager as supervisor
    return module, supervisor.get_broker(broker), supervisor.logger


def folder_path(value):
    if not os.path.isdir(value):
        raise argparse.ArgumentError("%r is not a valid directory" % value)
    return os.path.abspath(value)


def parse_arguments():
    parser = argparse.ArgumentParser(prog="croncodile", description="Run Inspecron workers.")
    parser.add_argument(
        "broker",
        help="Inspecron supports 2 brokers: rabbitmq(secondary), redis(primary)",
    )
    parser.add_argument(
        "modules", metavar="module", nargs="*",
        help="additional python modules to import",
    )
    parser.add_argument(
        "--processes", "-p", default=cpus, type=int,
        help="the number of worker processes to run (default: %s)" % cpus,
    )
    parser.add_argument(
        "--threads", "-t", default=8, type=int,
        help="the number of worker threads per process (default: 8)",
    )

    if HAS_WATCHDOG:
        parser.add_argument(
            "--watch", type=folder_path,
            help=(
                "watch a directory and reload the workers when any source files "
                "change (this feature must only be used during development)"
            )
        )

    parser.add_argument("--version", action="version", version=__version__)
    parser.add_argument("--verbose", "-v", action="count", default=0)
    return parser.parse_args()


def worker_process(args, worker_id, logging_fd):
    module, broker, _logger = import_broker(args.broker)
    try:
        logging_pipe = os.fdopen(logging_fd, "w")

        for module in args.modules:
            importlib.import_module(module)

        worker = Worker(broker, logger('Worker %d' % worker_id), worker_threads=args.threads)
        worker.start()
    except ImportError:
        _logger.exception("Failed to import module.")
        return os._exit(2)
    except ConnectionError:
        _logger.exception("Broker connection failed. for worker %s" % worker_id)
        return os._exit(3)

    def termhandler(signum, frame):
        global running
        if running:
            _logger.info("Stopping worker process...")
            running = False
        else:
            _logger.warning("Killing worker process...")
            return os._exit(1)

    _logger.info("Worker process is ready for action.")
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    signal.signal(signal.SIGTERM, termhandler)
    signal.signal(signal.SIGHUP, termhandler)

    running = True
    while running:
        time.sleep(1)

    worker.stop()
    broker.close()
    logging_pipe.close()


worker_processes = []


def main():
    args = parse_arguments()
    worker_pipes = []
    for worker_id in range(args.processes):
        read_fd, write_fd = os.pipe()
        pid = os.fork()
        if pid != 0:
            os.close(write_fd)
            worker_pipes.append(os.fdopen(read_fd))
            worker_processes.append(pid)
            continue

        os.close(read_fd)
        return worker_process(args, worker_id, write_fd)

    running, reload_process = True, False

    def sighandler(signum, frame):
        global reload_process, worker_processes
        reload_process = signum == signal.SIGHUP
        signum = {
            signal.SIGINT: signal.SIGTERM,
            signal.SIGTERM: signal.SIGTERM,
            signal.SIGHUP: signal.SIGHUP,
        }[signum]

        for pid in worker_processes:
            try:
                os.kill(pid, signum)
            except OSError:
                raise

    retcode = 0
    signal.signal(signal.SIGINT, sighandler)
    signal.signal(signal.SIGTERM, sighandler)
    signal.signal(signal.SIGHUP, sighandler)
    for pid in worker_processes:
        pid, rc = os.waitpid(pid, 0)
        retcode = max(retcode, rc >> 8)

    running = False

    for pipe in worker_pipes:
        pipe.close()

    if reload_process:
        if sys.argv[0].endswith("/croncodile/__main__.py"):
            return os.execvp("python", ["python", "-m", "croncodile", sys.argv[1:]])
        return os.execvp(sys.argv[0], sys.argv)
    return retcode


if __name__ == "__main__":
    sys.exit(main())
