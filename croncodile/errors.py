class InspecronError(Exception):  # pragma: no cover
    """Base class for all Inspecron errors.
    """

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return str(self.message) or repr(self.message)


class UndefinedBrokerError(InspecronError):
    """In case no broker found, we raise this for worker to handle it through Sqlite"""


class BrokerError(InspecronError):
    """Base class for broker-related errors.
    """


class ActorNotFound(BrokerError):
    """Raised when a message is sent to an actor that hasn't been declared.
    """


class QueueNotFound(BrokerError):
    """Raised when a message is sent to an queue that hasn't been declared.
    """


class ConnectionError(BrokerError):
    """Base class for broker connection-related errors.
    """


class ConnectionFailed(ConnectionError):
    """Raised when a broker connection could not be opened.
    """


class ConnectionClosed(ConnectionError):
    """Raised when a broker connection is suddenly closed.
    """


class RateLimitExceeded(InspecronError):
    """Raised when a rate limit has been exceeded.
    """
