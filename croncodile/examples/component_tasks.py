import croncodile
from croncodile import logger
from croncodile.core.task import GenericActor

croncodile.init_croncodile()
_logger = logger('task')


@croncodile.actor(actor_name='square', queue_name='note_fine_10_queue', max_retries=5)
def square(x):
    if x % 10 == 0:
        raise RuntimeError('My exception')
    return x * x


class ProcessingTask(GenericActor):
    class Meta:
        actor_name = "process_task"
        queue_name = "process_task"
        max_retries = 10

    def perform(self, model, role):
        """
        :type model: ModelData
        :param model: Model
        :type role: str
        :param role: New role
        :return:
        """
        model.promote(role)


class ModelData(object):
    description = "Basic"

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def promote(self, role):
        self.description = role
