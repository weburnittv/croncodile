"""Broker"""
from croncodile.errors import ActorNotFound


class Broker(object):
    """Abstract Broker to deal with different Queue redis,rabbitmq"""
    next_broker = None
    actors = {}
    queues = {}
    delay_queues = set()
    middleware = []

    def delegator(self, broker=None):
        """
        Set next delegator
        :type broker: Broker | None
        :param broker: Next broker
        """
        if self.next_broker:
            self.next_broker.delegator(broker)
        else:
            self.next_broker = broker
        return self.next_broker

    def enqueue(self, message, delay=None):
        """Implement concrete enqueue function"""

        raise NotImplementedError('This broker must implement this function')

    def consume(self):
        """Consume Queue"""
        raise NotImplementedError('This broker must implement this function to consume message from target Queue')

    def broadcast(self, message, delay=None):
        """
        Try to broadcast message
        :param delay: int
        :type message: Message
        :param message: Message to broadcast
        :return:
        """
        try:
            return self.enqueue(message, delay)
        except Exception as e:
            if self.next_broker:
                return self.next_broker.broadcast(message, delay)
            else:
                import croncodile.brokers.model as model
                model.connect()
                model.save(message)
                """
                @TODO: kick start Broker Watcher
                """
                return False

    def declare_actor(self, ac):  # pragma: no cover
        """Declare a new actor on this broker.  Declaring an Actor
        twice replaces the first actor with the second by name.

        Parameters:
          ac(Actor): The actor being declared.
        """
        self.actors[ac.actor_name] = ac
        self.declare_queue(ac.queue_name)
        if self.next_broker:
            self.next_broker.declare_actor(ac)

    def declare_queue(self, queue_name):  # pragma: no cover
        """Declare a queue on this broker.  This method must be
        idempotent.

        Parameters:
          queue_name(str): The name of the queue being declared.
        """
        raise NotImplementedError

    def get_declared_actors(self):  # pragma: no cover
        """Get all declared actors.

        Returns:
          set[str]: The names of all the actors declared so far on
          this Broker.
        """
        return set(self.actors.keys())

    def get_declared_queues(self):  # pragma: no cover
        """Get all declared queues.

        Returns:
          set[str]: The names of all the queues declared so far on
          this Broker.
        """
        return set(self.queues.keys())

    def get_declared_delay_queues(self):  # pragma: no cover
        """Get all declared delay queues.

        Returns:
          set[str]: The names of all the delay queues declared so far
          on this Broker.
        """
        return self.delay_queues.copy()

    def get_actor(self, actor_name):  # pragma: no cover
        """Look up an actor by its name.

        Parameters:
          actor_name(str): The name to look up.

        Raises:
          ActorNotFound: If the actor was never declared.

        Returns:
          Actor: The actor.
        """
        try:
            return self.actors[actor_name]
        except KeyError:
            raise ActorNotFound(actor_name)


class Consumer:
    """Consumers iterate over messages on a queue.

    Consumers and their MessageProxies are *not* thread-safe.
    """

    def __iter__(self):  # pragma: no cover
        """Returns this instance as a Message iterator.
        """
        return self

    def ack(self, message):  # pragma: no cover
        """Acknowledge that a message has been processed, removing it
        from the broker.

        Parameters:
          message(MessageProxy): The message to acknowledge.
        """
        raise NotImplementedError

    def nack(self, message):  # pragma: no cover
        """Move a message to the dead-letter queue.

        Parameters:
          message(MessageProxy): The message to reject.
        """
        raise NotImplementedError

    def requeue(self, messages):  # pragma: no cover
        """Move unacked messages back to their queues.  This is called
        by consumer threads when they fail or are shut down.

        Parameters:
          messages(list[MessageProxy]): The messages to requeue.
        """
        raise NotImplementedError

    def next(self):  # pragma: no cover
        """Retrieve the next message off of the queue.  This method
        blocks until a message becomes available.

        Returns:
          MessageProxy: A transparent proxy around a Message that can
          be used to acknowledge or reject it once it's done being
          processed.
        """
        raise NotImplementedError

    def interrupt(self):
        """Wake up the consumer if it is idling.
        """

    def close(self):
        """Close this consumer and perform any necessary cleanup actions.
        """


class MessageProxy(object):
    """Base class for messages returned by :meth:`Broker.consume`.
    """

    def __init__(self, message):
        self.failed = False
        self._message = message

    def fail(self):
        """Mark this message for rejection.
        """
        self.failed = True
        return self

    def __getattr__(self, name):
        return getattr(self._message, name)

    def __str__(self):
        return str(self._message)  # pragma: no cover

    def __lt__(self, other):
        # This can get called if two messages have the same priority
        # in a queue.  If that's the case, we don't care which runs
        # first.
        return True  # pragma: no cover

    def __eq__(self, other):
        if isinstance(other, MessageProxy):
            return self._message == other._message
        return self._message == other  # pragma: no cover
