"""Inspecron Module"""
import logging

import os
from pythonjsonlogger import jsonlogger

import croncodile.brokers.irabbitmq as rabbitmq
import croncodile.brokers.iredis as redis
import croncodile.core.player as player


def logger(mod_name):
    _logger = logging.getLogger(mod_name)
    handler = logging.StreamHandler()
    formatter = jsonlogger.JsonFormatter()

    handler.setFormatter(formatter)
    _logger.addHandler(handler)
    _logger.setLevel(logging.DEBUG)
    return _logger


def actor(fn=None, actor_name=None, queue_name="default", priority=0, max_retries=5, **kwargs):
    """Declare an actor.
    :return: player.Actor
    """

    def decorator(fn):
        supervisor = InspectorSupervisor()
        major_broker = supervisor.get_broker()

        if max_retries > 0 and int(kwargs.get('max_retries', 0)) == 0:
            kwargs['max_retries'] = max_retries
        actor = player.Actor(
            fn, actor_name=actor_name, queue_name=queue_name,
            priority=priority, broker=major_broker, options=kwargs
        )

        return actor

    if fn is None:
        return decorator
    return decorator(fn)


class InspectorSupervisor(object):
    """Manager which handle broker"""
    _instance = None

    def __new__(cls, *args, **kwargs):
        """
        Assure we have single one
        :return: _Singleton
        """
        if InspectorSupervisor._instance is None:
            InspectorSupervisor._instance = InspectorSupervisor._Singleton(*args, **kwargs)

        InspectorSupervisor._instance.init(*args, **kwargs)
        return InspectorSupervisor._instance

    class _Singleton(object):
        """Assure supervisor is single"""
        broker = None
        actors = {}
        _logger = None
        brokers = {}

        def __init__(self, default_broker=None, logger=None):
            """Set default broker"""
            self.broker = default_broker
            self._logger = logger

        def init(self, *args, **kwargs):
            if 'logger' in kwargs:
                self._logger = kwargs['logger']

        def register_broker(self, broker, name):
            """
            Register broker to chain
            :param broker: Broker
            :param broker: Broker which deals with particular Queue system
            :return:
            """
            self.brokers[name] = broker
            if self.broker:
                self.broker.delegator(broker)
                return
            self.broker = broker

        def get_broker(self, broker_name=None):
            """
            Return Broker
            :return: Broker
            """
            if broker_name is not None and broker_name in self.brokers:
                return self.brokers[broker_name]
            return self.broker

        @property
        def logger(self):
            return self._logger


default_config = {
    'redis': {
        'host': os.environ.get('REDIS_HOST', 'sentinel'),
        'port': os.environ.get('REDIS_PORT', 26379)
    },
    'rabbitmq': {
        'url': os.environ.get('RABBITMQ_URL'),
        'host': os.environ.get('RABBITMQ_HOST', 'rabbitmq'),
        'port': int(os.environ.get('RABBITMQ_PORT', 5672)),
        'username': os.environ.get('RABBITMQ_DEFAULT_USER', 'guest'),
        'password': os.environ.get('RABBITMQ_DEFAULT_PASS', 'guest'),
    }
}


def create_broker(name, logger, **kwargs):
    """Create Broker"""
    brokers = {
        'redis': lambda x: redis.Redis(logger=logger, **kwargs),
        'rabbitmq': lambda x: rabbitmq.RabbitMQ(logger=logger, **kwargs)
    }
    if len(kwargs):
        return brokers[name](default_config)

    return None


manager = None
getLogger = logger


def init_croncodile(logger=None, config=None):
    """
    Init Brokers according to current env
    :return:
    """
    if logger is None:
        logger = getLogger('Inspecron')
    global manager
    manager = InspectorSupervisor(logger=logger)
    import utils
    if int(os.environ.get('AUTO_DISCOVER_CONFIG', 0)) > 0:
        config = default_config
    if config:
        for k, v in config.iteritems():
            v = utils.filter_dictionary(v)
            broker = create_broker(k, logger=logger, **v)
            if broker:
                manager.register_broker(broker, k)

    return manager


__version__ = "0.0.1"
