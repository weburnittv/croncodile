"""Sqlite Model"""
from peewee import *
import os
from playhouse.apsw_ext import APSWDatabase
import uuid
import datetime
import cPickle as pickle

db = APSWDatabase("inspector.db")
connected = False


class BaseModel(Model):
    """Sqlite Base Model"""

    class Meta:
        database = db


class NodeRecord(BaseModel):
    """Failed message which will be record in sqlite"""
    content = TextField()
    action = TextField()
    sync = BooleanField()
    created_at = TimestampField()
    id = UUIDField(primary_key=True)

    class Meta:
        created_at = datetime.datetime.now().time()

    def message(self):
        """Deserialize by pickle"""
        return pickle.loads(str(self.content))


class Connector:
    """Connector to Sqlite"""

    def __init__(self):
        global connected
        if not connected:
            db.connect()
            connected = True

    @staticmethod
    def add(content, action="enqueue"):
        """
        Add new data instance
        :return: NodeRecord
        """
        content = pickle.dumps(content)
        record = NodeRecord(content=content, action=action, sync=False, id=uuid.uuid4())
        record.save(force_insert=True)
        return record

    @staticmethod
    def delete(id):
        """
        Delete Instance
        :param id: UUID
        """
        instance = NodeRecord.get(NodeRecord.id == id)
        instance.delete_instance()

    @staticmethod
    def select():
        instances = NodeRecord.select()
        return instances


connector = None


def connect():
    """Propose save function directly"""
    global connector, db
    if not os.path.exists(os.getcwd() + "/inspector.db"):
        db.connect()
        db.create_tables([NodeRecord])
        db.close()
    if not connector:
        connector = Connector()
    return connector, db


def save(message, action="enqueue"):
    connection, db = connect()
    connection.add(message, action)
