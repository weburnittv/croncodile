"""From component side, just use Task and Actor(decorator) to send parameters"""
import sys

from croncodile.examples.component_tasks import ModelData, ProcessingTask, square

slaver = ModelData(sys.argv[1], sys.argv[2])

if __name__ == '__main__':
    for i in range(100):
        slaver.age = i
        ProcessingTask.send(slaver, sys.argv[3])
        square.send(i)
