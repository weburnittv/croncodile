#!/usr/bin/env bash
cd /var/www/html/application
pip install -r requirements.txt
pip install -e .
python -m croncodile app